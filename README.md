# beets and roots
Sample application

## Setup
```bash
yarn install
```

## Start app
```bash
yarn dev
```

## Run test
```bash
yarn test
```

## How did you decide on the technical and architectural choices used as part of your solution?
- I "assumed" the api data could change on every request firstly, even tho that's not the case at the moment so the data is fetched every time on page load.
- I wanted to have as little as possible code duplication, so I used HOC and defined a config where I saw necessary.
- I also wanted to keep similar code blocks and components together in the same directory, and only move components to a new directory as the app grew "*a bit :-)*".
- I didn't really think of routes as routes, more like components thanks to react-router :)

## Are there any improvements you could make to your submission?
- More test!!!

## What would you do differently if you were allocated more time?
- Differently? Try to futher optimise the render where need be.
