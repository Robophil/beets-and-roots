import React from 'react'

const HORSE_POWER = 200
const LIMIT = 20

export default function withData (origin) {
  return function (Component) {
    return class extends React.PureComponent {
      state = {
        isLoading: false,
        data: [],
        error: null
      }

      componentDidMount () {
        this.setState({ isLoading: true })
        fetch('https://raw.githubusercontent.com/vega/vega/master/docs/data/cars.json')
          .then(response => response.json())
          .then(data => this.filter(data))
          .then(data => this.setState({ data: [...data], error: null }))
          .then(() => this.setState({ isLoading: false }))
          .catch(error => {
            this.setState({ error, isLoading: false })
            console.log(error)
            throw error
          })
      }

      filter (data) {
        return data.filter(car => (car.Origin === origin && car.Horsepower <= HORSE_POWER))
          .sort((carA, carB) => {
            const dateA = new Date(carA.Year)
            const dateB = new Date(carB.Year)

            if (dateA < dateB) {
              return -1
            }
            if (dateA > dateB) {
              return 1
            }

            return 0
          }).slice(0, LIMIT)
      }

      toColumns (keys) {
        return keys.map(row => ({
          sortable: true,
          name: row,
          selector: row
        }))
      }

      render () {
        const { data, isLoading, error } = this.state
        const keys = data.length ? Object.keys(data[0]) : []
        const props = {
          cars: data,
          columns: this.toColumns(keys),
          ...this.props
        }

        if (isLoading) {
          return <label>Loading...</label>
        }

        if (error !== null) {
          return (
            <div>
              Could not download
              Error: ${error.message}
            </div>
          )
        }

        return (
          <Component {...props} />
        )
      }
    }
  }
}
