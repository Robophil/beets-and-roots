import React from 'react'
import { render } from '@testing-library/react'
import PageContent from './PageContent'

describe('Content', () => {
  it('renders the same snapshot', () => {
    const props = {
      title: 'cars',
      columns: [
        {
          sortable: true,
          name: 'Name',
          selector: 'name'
        },
        {
          sortable: true,
          name: 'Price',
          selector: 'price'
        }
      ],
      cars: [
        { name: 'BMW', price: '€100.000' },
        { name: 'Toyota', price: '€48.000' }
      ]
    }
    const response = render(<PageContent {...props} />)
    expect(response).toBeTruthy()
  })
})
