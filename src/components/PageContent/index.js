import {
  USAPage,
  EuropePage,
  JapanPage
} from './PageContent'

export {
  USAPage,
  EuropePage,
  JapanPage
}
