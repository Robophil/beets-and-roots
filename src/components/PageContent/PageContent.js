import React from 'react'
import DataTable from 'react-data-table-component'
import withData from './withData'

export default class PageContent extends React.PureComponent {
  render () {
    return (
      <DataTable
        title={this.props.title}
        columns={this.props.columns}
        data={this.props.cars}
      />
    )
  }
}

const USAPage = withData('USA')(PageContent)
const EuropePage = withData('Europe')(PageContent)
const JapanPage = withData('Japan')(PageContent)

export {
  USAPage,
  EuropePage,
  JapanPage
}
