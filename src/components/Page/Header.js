import React from 'react'
import { withRouter } from 'react-router'
import Link from './Link'
import routes from './routes'

import './Header.css'

export class Header extends React.PureComponent {
  render () {
    return (
      <header>
        <nav className='nav'>
          {routes.map((route, index) => <Link key={index} to={route.path} isActive={route.path === this.props.location.pathname}>{route.label}</Link>)}
        </nav>
      </header>
    )
  }
}

export default withRouter(Header)
