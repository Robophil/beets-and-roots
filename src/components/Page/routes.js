import React from 'react'
import {
  USAPage,
  EuropePage,
  JapanPage
} from '../PageContent'

const routes = [
  { path: '/', label: 'Home', Component: () => <div>Welcome to Cars!!</div> },
  { path: '/Europe', label: 'Europe', Component: EuropePage },
  { path: '/USA', label: 'USA', Component: USAPage },
  { path: '/JAPAN', label: 'Japan', Component: JapanPage }
]

export default routes
