import React from 'react'
import { render } from '@testing-library/react'
import {
  BrowserRouter as Router
} from 'react-router-dom'
import Page from './Page'

describe('Page', () => {
  it('renders the same snapshot', () => {
    const response = render(<Router><Page /></Router>)
    expect(response).toMatchSnapshot()
  })
})
