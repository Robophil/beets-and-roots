import React from 'react'

import Header from './Header'
import Content from './Content'
import Footer from './Footer'

import './Page.css'

export default class Page extends React.PureComponent {
  render () {
    return (
      <>
        <Header />
        <Content />
        <Footer />
      </>
    )
  }
}
