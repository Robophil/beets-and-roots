import React from 'react'
import { render } from '@testing-library/react'
import {
  BrowserRouter as Router
} from 'react-router-dom'
import Content from './Content'

describe('Content', () => {
  it('renders the same snapshot', () => {
    const response = render(<Router><Content /></Router>)
    expect(response).toMatchSnapshot()
  })
})
