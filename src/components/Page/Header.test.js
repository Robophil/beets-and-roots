import React from 'react'
import { render } from '@testing-library/react'
import {
  BrowserRouter as Router
} from 'react-router-dom'
import { Header } from './Header'

describe('Header', () => {
  it('renders the same snapshot', () => {
    const props = {
      location: {
        pathname: '/'
      }
    }
    const response = render(<Router><Header {...props} /></Router>)
    expect(response).toMatchSnapshot()
  })
})
