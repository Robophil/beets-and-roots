import React from 'react'

import './Footer.css'

export default class Footer extends React.PureComponent {
  render () {
    return (
      <footer className='footer'>
        <div>© 2020 All Rights Reserved</div>
      </footer>
    )
  }
}
