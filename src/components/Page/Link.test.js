import React from 'react'
import { render } from '@testing-library/react'
import {
  BrowserRouter as Router
} from 'react-router-dom'
import Link from './Link'

describe('Link', () => {
  it('renders the same snapshot', () => {
    const props = {
      to: '/',
      isActive: true
    }
    const response = render(<Router><Link {...props} /></Router>)
    expect(response).toMatchSnapshot()
  })
})
