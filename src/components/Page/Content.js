import React from 'react'
import {
  Switch,
  Route
} from 'react-router-dom'
import routes from './routes'

export default class Content extends React.PureComponent {
  render () {
    return (
      <main>
        <Switch>
          {routes.map((route, index) => (
            <Route exact key={index} path={route.path} render={() => <route.Component title={route.label} />} />
          ))}
        </Switch>
      </main>
    )
  }
}
