import React from 'react'
import { render } from '@testing-library/react'
import Footer from './Footer'

describe('Footer', () => {
  it('renders the same snapshot', () => {
    const response = render(<Footer />)
    expect(response).toMatchSnapshot()
  })
})
