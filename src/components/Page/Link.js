import React from 'react'
import {
  Link as RouterLink
} from 'react-router-dom'

import './Link.css'

export default class Link extends React.PureComponent {
  render () {
    const { isActive, ...otherProps } = this.props
    return (
      <RouterLink className='link' {...otherProps}>
        {isActive ? '> ' : ''}{this.props.children}
      </RouterLink>
    )
  }
}
