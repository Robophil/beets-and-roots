import React from 'react'
import { render } from '@testing-library/react'
import App from './App'

describe('App', () => {
  it('renders the same snapshot', () => {
    const response = render(<App />)
    expect(response).toMatchSnapshot()
  })
})
