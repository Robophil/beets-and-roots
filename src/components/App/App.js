import React from 'react'
import {
  BrowserRouter as Router
} from 'react-router-dom'

import { Page } from '../Page'
import './App.css'

function App () {
  return (
    <Router>
      <Page />
    </Router>
  )
}

export default App
